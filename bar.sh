keyboard_input_name="1:1:AT_Translated_Set_2_keyboard"

current_date=$(date "+%Y/%m/%d")
current_time=$(date "+%H:%M")

battery_charge=$(cat /sys/class/power_supply/BAT0/status)
battery_status=$(cat /sys/class/power_supply/BAT0/capacity)

audio_volume=$(awk -F"[][]" '/Left:/ { print $2 }' <(amixer sget Master))

network=$(ip route get 1.1.1.1 | grep -Po '(?<=dev\s)\w+' | cut -f1 -d ' ')

language=$(swaymsg -r -t get_inputs | awk '/1:1:AT_Translated_Set_2_keyboard/;/xkb_active_layout_name/' | grep -A1 '\b1:1:AT_Translated_Set_2_keyboard\b' | grep "xkb_active_layout_name" | awk -F '"' '{print $4}')

echo "⌨ $language | $network_active $network | 🔈 $audio_volume | 🗲 $battery_charge $battery_status% | 🕘 $current_date $current_time"
